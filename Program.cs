﻿using System;

namespace LateRide
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            System.Console.WriteLine(lateRide(808));
            //14
            System.Console.WriteLine(lateRide(1439));
            //19
            System.Console.WriteLine(lateRide(0));
            //0
            System.Console.WriteLine(lateRide(23));
            // 5
            System.Console.WriteLine(lateRide(194));
            //8
            System.Console.WriteLine(lateRide(619));
            //11
            System.Console.WriteLine(lateRide(1003));
            //4
            System.Console.WriteLine(lateRide(55));
            //10
            System.Console.WriteLine(lateRide(391));
            //10
            System.Console.WriteLine(lateRide(3));
            //3

        }

        static int lateRide(int n) {
            if(n >= 10 && n < 60){
                return (n / 10) + (n % 10);
            } else {
                int hours = n / 60;
                int leftOverMinutes = n % 60;
                
                if (hours >= 10){
                    if(leftOverMinutes >= 10){
                        return ((hours / 10) + (hours % 10) + (leftOverMinutes / 10) + (leftOverMinutes % 10));
                    }else{
                        return ((hours / 10) + (hours % 10) + leftOverMinutes);
                    }
                }else{
                    if(leftOverMinutes >= 10){
                        return (hours + (leftOverMinutes / 10) + (leftOverMinutes % 10));
                    }else{
                        return (hours + leftOverMinutes);
                    }
                }
            }
        }


    }
}
